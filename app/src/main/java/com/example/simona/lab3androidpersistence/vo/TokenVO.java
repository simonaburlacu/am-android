package com.example.simona.lab3androidpersistence.vo;

import java.io.Serializable;

public class TokenVO implements Serializable
{
    private String token;

    public TokenVO(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
