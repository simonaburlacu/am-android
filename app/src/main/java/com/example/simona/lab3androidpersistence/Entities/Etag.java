package com.example.simona.lab3androidpersistence.Entities;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "Etags")
public class Etag {
    @PrimaryKey
    @NonNull
    String key;
    String value;

    @NonNull
    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setKey(@NonNull String key) {
        this.key = key;
    }

    public Etag(@NonNull String key, String value) {
        this.key = key;
        this.value = value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}