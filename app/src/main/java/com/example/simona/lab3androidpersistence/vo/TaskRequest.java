package com.example.simona.lab3androidpersistence.vo;

public class TaskRequest
{
    private String Description;
    private int Id;

    public String getDescription() {
        return Description;
    }

    public int getId() {
        return Id;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setId(int id) {
        Id = id;
    }

    public TaskRequest(String description, int id) {
        Description = description;
        Id = id;
    }

    public TaskRequest() { }

}
