package com.example.simona.lab3androidpersistence.ViewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.support.annotation.NonNull;

import com.example.simona.lab3androidpersistence.Database.UserDatabase;
import com.example.simona.lab3androidpersistence.Entities.Etag;
import com.example.simona.lab3androidpersistence.Entities.Task;
import com.example.simona.lab3androidpersistence.Entities.Token;
import com.example.simona.lab3androidpersistence.Repository.ETagRepository;
import com.example.simona.lab3androidpersistence.Repository.TaskRepository;
import com.example.simona.lab3androidpersistence.vo.TaskRequest;
import com.example.simona.lab3androidpersistence.vo.TokenVO;

import java.util.List;

public class TaskViewModel extends ViewModel
{

    private LiveData<List<Task>> taskLiveData;
    private TaskRepository taskRepository;
    private ETagRepository eTagRepository;

    public TaskViewModel(Context context)
    {
        taskRepository=TaskRepository.getInstance(UserDatabase.getDatabase(context).getTaskDao(),UserDatabase.getDatabase(context).getEtagDao(), UserDatabase.getDatabase(context).getTokenDao());
        eTagRepository=ETagRepository.getInstance(UserDatabase.getDatabase(context).getEtagDao());
        taskLiveData = taskRepository.getCacheTasks();
    }

    public LiveData<List<Task>> getTasksLiveData() {
        return taskLiveData;
    }

    public void getTasks(Token token)
    {
        Etag etagTasks=eTagRepository.getEtagValue("etagTasks");
        taskRepository.getTasks(token,etagTasks);
    }

    public Etag getEtag(String key)
    {
        return eTagRepository.getEtagValue(key);
    }

    public void editTask(int id, String desc, TokenVO tokenVO) {
        TaskRequest taskRequest=new TaskRequest(desc,id);
        taskRepository.update(taskRequest,new Token(tokenVO.getToken()));
    }

    public static class Factory implements ViewModelProvider.Factory {
        private final Context ctxt;

        public Factory(Context ctxt) {
            this.ctxt=ctxt.getApplicationContext();
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return((T)new TaskViewModel(ctxt));
        }
    }
}

