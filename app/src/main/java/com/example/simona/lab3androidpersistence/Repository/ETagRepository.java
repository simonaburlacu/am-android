package com.example.simona.lab3androidpersistence.Repository;
import com.example.simona.lab3androidpersistence.DAO.EtagsDao;
import com.example.simona.lab3androidpersistence.Entities.Etag;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ETagRepository
{
    private EtagsDao etagsDao;
    private static ETagRepository repositoryInstance;
    Executor executor;

    private ETagRepository(EtagsDao etagsDao)
    {
        this.etagsDao=etagsDao;
        executor = Executors.newSingleThreadExecutor();
    }

    public static ETagRepository getInstance(EtagsDao etagsDao)
    {
        if(repositoryInstance==null)
        {
            repositoryInstance=new ETagRepository(etagsDao);
        }
        return repositoryInstance;
    }

    public Etag getEtagValue(String key)
    {
        return etagsDao.getEtag(key);
    }
}
