package com.example.simona.lab3androidpersistence.Repository;
import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.util.Log;
import com.example.simona.lab3androidpersistence.API.APIservice;
import com.example.simona.lab3androidpersistence.DAO.TokenDao;
import com.example.simona.lab3androidpersistence.DAO.UserDao;
import com.example.simona.lab3androidpersistence.Entities.Token;
import com.example.simona.lab3androidpersistence.vo.AccountRequest;
import com.example.simona.lab3androidpersistence.vo.TokenResponse;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserRepository
{
    private static final String CLASSNAME = "USER REPOSITORY";
    private UserDao userDao;
    private TokenDao tokenDao;
    private static UserRepository repositoryInstance;
    Executor executor;

    private UserRepository(UserDao userDao, TokenDao tokenDao)
    {
        this.tokenDao=tokenDao;
        this.userDao=userDao;
        executor = Executors.newSingleThreadExecutor();
    }

    public static  UserRepository getInstance(UserDao userDao, TokenDao tokenDao)
    {
        if(repositoryInstance==null)
        {
            repositoryInstance=new UserRepository(userDao, tokenDao);
        }
        return repositoryInstance;
    }

    private APIservice getAPIService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIservice.MY_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIservice api = retrofit.create(APIservice.class);
        return api;
    }

    public void login(String username, String password,final Function<String,String> callback)
    {
        getAPIService()
                .login(new AccountRequest(username,password))
                .enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(Call<TokenResponse> call, final Response<TokenResponse> response) {
                Log.d(CLASSNAME,"CALL LOGIN SUCCESS");
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        if(response.body()!=null) {
                            String token = response.body().getToken();
                            if (token != "") {
                                Token insertToken = new Token(token);
                                tokenDao.insert(insertToken);
                            }
                        }
                        else
                        {
                            callback.apply("unauthorized");
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {
                Log.d(CLASSNAME,"CALL LOGIN FAILED",t);
                callback.apply("unauthorized");
            }
        });

    }

    public LiveData<Token> getToken() {
        return tokenDao.getToken();
    }

    public void clearToken() {
        executor.execute(new Runnable() {
         @Override
         public void run() {
             tokenDao.deleteToken();
         }
        });
    }

}
