package com.example.simona.lab3androidpersistence.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import com.example.simona.lab3androidpersistence.Entities.Token;

@Dao
public interface TokenDao {
    @Insert
    void insert(Token token);

    @Query("SELECT * FROM Tokens LIMIT 1")
    LiveData<Token> getToken();

    @Query("SELECT * FROM Tokens LIMIT 1")
    Token getNoTrackToken();

    @Query("DELETE FROM Tokens")
    void deleteToken();

}
