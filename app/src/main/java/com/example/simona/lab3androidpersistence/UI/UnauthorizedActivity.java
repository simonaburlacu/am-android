package com.example.simona.lab3androidpersistence.UI;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import com.example.simona.lab3androidpersistence.R;

public class UnauthorizedActivity extends AppCompatActivity
{
    private TextView text_error;

    private void initControls()
    {
        text_error=(TextView)findViewById(R.id.error);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.unauthorized_layout);
        initControls();
    }
}
