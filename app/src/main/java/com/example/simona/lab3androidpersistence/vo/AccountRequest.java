package com.example.simona.lab3androidpersistence.vo;

public class AccountRequest
{
    private String Username;
    private String Password;

    public String getUsername() {
        return Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public AccountRequest(String username, String password) {
        Username = username;
        Password = password;
    }

    public AccountRequest()
    {

    }
}
