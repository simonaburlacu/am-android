package com.example.simona.lab3androidpersistence.DAO;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import com.example.simona.lab3androidpersistence.Entities.Etag;

@Dao
public interface EtagsDao {

    @Insert
    void insert(Etag etag);

    @Update
    void update(Etag etag);

    @Query("SELECT * FROM Etags WHERE Etags.`key` LIKE :searchKey")
    Etag getEtag(String searchKey);
}