package com.example.simona.lab3androidpersistence.UI;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.example.simona.lab3androidpersistence.API.APIservice;
import com.example.simona.lab3androidpersistence.Entities.Task;
import com.example.simona.lab3androidpersistence.Entities.Token;
import com.example.simona.lab3androidpersistence.R;
import com.example.simona.lab3androidpersistence.Utils.NetworkChangeReceiver;
import com.example.simona.lab3androidpersistence.ViewModels.TaskViewModel;
import com.example.simona.lab3androidpersistence.ViewModels.UserViewModel;
import com.example.simona.lab3androidpersistence.vo.TaskEdit;
import com.example.simona.lab3androidpersistence.vo.TokenVO;
import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import com.microsoft.signalr.HubConnectionState;

import java.util.Collections;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
    private TextView text_welcome;
    private TaskViewModel taskViewModel;
    private Button button_logout;
    private ListView listView;
    private static final String CLASSNAME = "HOME ACTIVITY";
    private List<Task> existingTasks=null;
    TaskListAdapter adapter;
    private Token existingToken=null;
    private UserViewModel userViewModel;
    private SensorManager mSensorManager;
    private float mAccel; // acceleration apart from gravity
    private float mAccelCurrent; // current acceleration including gravity
    private float mAccelLast; // last acceleration including gravity
    final Context context=this;
    private HubConnection hubConnection;

    private final SensorEventListener mSensorListener = new SensorEventListener() {
        public void onSensorChanged(SensorEvent se) {
            float x = se.values[0];
            float y = se.values[1];
            float z = se.values[2];
            mAccelLast = mAccelCurrent;
            mAccelCurrent = (float) Math.sqrt((double) (x*x + y*y + z*z));
            float delta = mAccelCurrent - mAccelLast;
            mAccel = mAccel * 0.9f + delta; // perform low-cut filter
            if (mAccel > 12 && existingTasks!=null && adapter!=null) {
                Collections.shuffle(existingTasks);
                TaskListAdapter newAdapter = new TaskListAdapter(context, existingTasks);
                listView.setAdapter(newAdapter);
            }
        }
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };
    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }
    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(mSensorListener);
        super.onPause();
    }

    private void initControls() {
        text_welcome = findViewById(R.id.welcome);
        text_welcome.setText("Welcome!");
        button_logout=findViewById(R.id.logout);
        listView = findViewById(R.id.listview_tasks);
    }

    private void initViewModels()
    {
        userViewModel=ViewModelProviders.of(this, new UserViewModel.Factory(getApplicationContext())).get(UserViewModel.class);
        taskViewModel = ViewModelProviders.of(this, new TaskViewModel.Factory(getApplicationContext())).get(TaskViewModel.class);
    }

    private void initSensorManager()
    {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;
    }

    private void initHubConnection()
    {
        hubConnection = HubConnectionBuilder.create(APIservice.MY_URL+"broadcaster")
                .build();

        hubConnection.on("SendMessage", (message) -> {
            if(existingToken!=null)
            {
                taskViewModel.getTasks(existingToken);
            }
        },Object.class);

        hubConnection.start();
        while(hubConnection.getConnectionState()!=HubConnectionState.CONNECTED){}
        hubConnection.send("Subscribe","User");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initControls();
        initViewModels();
        initSensorManager();
        initHubConnection();



        NetworkChangeReceiver networkListener=new NetworkChangeReceiver();

        userViewModel.getToken().observe(this, new Observer<Token>() {
            @Override
            public void onChanged(@Nullable Token token) {
                if (token != null) {
                    Log.d(CLASSNAME, "YOU ARE AUTHORIZED ");
                    existingToken=token;
                    taskViewModel.getTasks(existingToken);
                } else {
                    Log.d(CLASSNAME, "ACCESS DENIED");
                    Intent intent = new Intent(HomeActivity.this, UnauthorizedActivity.class);
                    startActivity(intent);
                    finish();
                    logoutUser();
                }
            }
        });


        taskViewModel.getTasksLiveData().observe(this, new Observer<List<Task>>() {
            @Override
            public void onChanged(List<Task> tasks) {
                existingTasks=tasks;
                adapter = new TaskListAdapter(context, tasks);
                listView.setAdapter(adapter);
            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Task item = (Task) parent.getItemAtPosition(position);
                TaskEdit taskEdit=new TaskEdit(item.getDescription(),item.getId());
                Intent intent = new Intent(context, EditActivity.class);
                intent.putExtra("TaskEdit", taskEdit);
                String sendToken="";
                if(existingToken!=null)
                    sendToken=existingToken.getToken();
                intent.putExtra("Token",new TokenVO(sendToken));
                startActivity(intent);
            }
        });

        button_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });
    }

    private void logoutUser() {
        hubConnection.send("Unsubscribe","User");
        hubConnection.stop();

        userViewModel.clearToken();
        Log.d(CLASSNAME,"LOG OUT OK");
        Intent intent = new Intent(HomeActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }


}