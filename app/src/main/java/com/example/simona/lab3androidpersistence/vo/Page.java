package com.example.simona.lab3androidpersistence.vo;

import java.util.List;

public class Page {
    List<TaskVO> Tasks;

    public List<TaskVO> getTaskVOS() {
        return Tasks;
    }

    public Page(List<TaskVO> tasks)
    {
        this.Tasks=tasks;
    }

    public Page(){}
}
