package com.example.simona.lab3androidpersistence.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import com.example.simona.lab3androidpersistence.Entities.Task;

import java.util.List;

@Dao
public interface TaskDao {
    @Query("SELECT * FROM Tasks")
    LiveData<List<Task>> getTasks();

    @Query("SELECT * FROM Tasks")
    List<Task> getNoTrackTasks();

    @Insert
    void insert(Task task);

    @Query("DELETE FROM Tasks")
    void deleteAll();

    @Update
    void update(Task task);

    @Query("SELECT * FROM Tasks WHERE Tasks.id LIKE :searchId")
    Task getTask(int searchId);
}
