package com.example.simona.lab3androidpersistence.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.simona.lab3androidpersistence.DAO.EtagsDao;
import com.example.simona.lab3androidpersistence.DAO.TaskDao;
import com.example.simona.lab3androidpersistence.DAO.TokenDao;
import com.example.simona.lab3androidpersistence.DAO.UserDao;
import com.example.simona.lab3androidpersistence.Entities.Etag;
import com.example.simona.lab3androidpersistence.Entities.Task;
import com.example.simona.lab3androidpersistence.Entities.Token;
import com.example.simona.lab3androidpersistence.Entities.User;

@Database(entities = {Token.class, Task.class, User.class, Etag.class},version = 1)
public abstract class UserDatabase extends RoomDatabase
{
    public static UserDatabase databaseInstance;

    public abstract TokenDao getTokenDao();

    public abstract UserDao getUserDao();

    public abstract TaskDao getTaskDao();

    public abstract EtagsDao getEtagDao();

    public static UserDatabase getDatabase(Context context)
    {
        if(databaseInstance==null)
        {
            databaseInstance=Room.databaseBuilder(context.getApplicationContext(),UserDatabase.class, "user-database").allowMainThreadQueries().build();
        }
        return databaseInstance;
    }

    public static void destroyInstance()
    {
        databaseInstance = null;
    }
}
