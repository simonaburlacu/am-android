package com.example.simona.lab3androidpersistence.Repository;

import android.arch.lifecycle.LiveData;
import android.util.Log;

import com.example.simona.lab3androidpersistence.API.APIservice;
import com.example.simona.lab3androidpersistence.DAO.EtagsDao;
import com.example.simona.lab3androidpersistence.DAO.TaskDao;
import com.example.simona.lab3androidpersistence.DAO.TokenDao;
import com.example.simona.lab3androidpersistence.Entities.Etag;
import com.example.simona.lab3androidpersistence.Entities.Task;
import com.example.simona.lab3androidpersistence.Entities.Token;
import com.example.simona.lab3androidpersistence.vo.Page;
import com.example.simona.lab3androidpersistence.vo.TaskRequest;
import com.example.simona.lab3androidpersistence.vo.TaskVO;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TaskRepository
{
    private static final String CLASSNAME = "TASK REPOSITORY";
    static private TaskDao taskDao;
    static private TokenDao tokenDao;
    private static TaskRepository repositoryInstance;
    static Executor executor;
    static private EtagsDao etagsDao;


    private TaskRepository(TaskDao taskDao, EtagsDao etagsDao,TokenDao tokenDao)
    {
        this.etagsDao=etagsDao;
        this.taskDao=taskDao;
        this.tokenDao=tokenDao;
        executor = Executors.newSingleThreadExecutor();
    }

    public static TaskRepository getInstance(TaskDao taskDao, EtagsDao etagsDao, TokenDao tokenDao)
    {
        if(repositoryInstance==null)
        {
            repositoryInstance=new TaskRepository(taskDao, etagsDao,tokenDao);
        }
        return repositoryInstance;
    }

    private static APIservice getAPIService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIservice.MY_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIservice api = retrofit.create(APIservice.class);
        return api;
    }


    public void getTasks(Token token, Etag etag)
    {
        //send etag to header
        String tokenString="";
        if(token!=null)
        {
            tokenString=token.getToken();
        }
        String authString="Bearer "+tokenString;
        String etagString="";
        if(etag!=null)
        {
            etagString=etag.getValue();
        }
        String etagHeader="etag "+etagString;
        final String etagSent=etagString;

        getAPIService()
                .getTasks(authString,etagHeader)
                .enqueue(new Callback<Page>() {
                    @Override
                    public void onResponse(Call<Page> call, final Response<Page> response) {
                        Log.d(CLASSNAME,"CALL GET TASKS SUCCESS");
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                if(response.body()!=null)
                                {
                                    List<TaskVO> tasks=response.body().getTaskVOS();
                                    String etagReceived=response.headers().get("etag");
                                    if(etagReceived!=etagSent) {
                                        taskDao.deleteAll();
                                        if (tasks != null) {
                                            for (TaskVO task : tasks) {
                                                Task taskDB = new Task(task.getId(), task.getDescription(), task.getPriority());
                                                taskDao.insert(taskDB);
                                            }
                                        }

                                        if(etagSent=="")
                                        {
                                            Etag etagAdded=new Etag("etagTasks",etagReceived);
                                            etagsDao.insert(etagAdded);
                                        }
                                        else
                                        {
                                            Etag etagUpdate=new Etag("etagTasks",etagReceived);
                                            etagsDao.update(etagUpdate);
                                        }
                                    }
                                }
                            }
                        });
                    }
                    @Override
                    public void onFailure(Call<Page> call, Throwable t) {
                        Log.d(CLASSNAME,"CALL GET TASKS FAILED",t);
                    }
                });
    }

    public void update(final TaskRequest taskRequest, Token token)
    {
        String tokenString="";
        if(token!=null)
        {
            tokenString=token.getToken();
        }
        String authString="Bearer "+tokenString;

        getAPIService()
                .update(taskRequest, authString)
                .enqueue(new Callback<TaskVO>() {
                    @Override
                    public void onResponse(Call<TaskVO> call, final Response<TaskVO> response) {
                        Log.d(CLASSNAME,"CALL UPDATE SUCCESS");
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                if(response.body()!=null) {
                                    TaskVO task = response.body();
                                    if (task != null) {
                                        Task taskDB = new Task(task.getId(), task.getDescription(), task.getPriority());
                                        taskDao.update(taskDB);
                                    }
                                }
                            }
                        });
                    }
                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Log.d(CLASSNAME,"CALL UPDATE FAILED",t);
                        Task existingTask=taskDao.getTask(taskRequest.getId());
                        Task newTask=new Task(taskRequest.getId(), taskRequest.getDescription(),existingTask.getPriority());
                        taskDao.update(newTask);
                    }
                });

    }

    public LiveData<List<Task>> getCacheTasks()
    {
        return taskDao.getTasks();
    }

    public static void sendOfflineTasks()
    {
        List<Task> currentTasks=taskDao.getNoTrackTasks();
        Token token=tokenDao.getNoTrackToken();
        String tokenString="";
        if(token!=null)
        {
            tokenString=token.getToken();
        }
        String authString="Bearer "+tokenString;
        for (Task task:currentTasks)
        {
            final TaskRequest taskRequest=new TaskRequest(task.getDescription(),task.getId());
            getAPIService()
                    .update(taskRequest, authString)
                    .enqueue(new Callback<TaskVO>() {
                        @Override
                        public void onResponse(Call<TaskVO> call, final Response<TaskVO> response) {
                            Log.d(CLASSNAME,"CALL UPDATE SUCCESS");
                            executor.execute(new Runnable() {
                                @Override
                                public void run() {
                                    if(response.body()!=null) {
                                        TaskVO task = response.body();
                                        if (task != null) {
                                            Task taskDB = new Task(task.getId(), task.getDescription(), task.getPriority());
                                            taskDao.update(taskDB);
                                        }
                                    }
                                }
                            });
                        }
                        @Override
                        public void onFailure(Call call, Throwable t) {
                            Log.d(CLASSNAME,"CALL UPDATE FAILED",t);
                            Task existingTask=taskDao.getTask(taskRequest.getId());
                            Task newTask=new Task(taskRequest.getId(), taskRequest.getDescription(),existingTask.getPriority());
                            taskDao.update(newTask);
                        }
                    });
        }
    }
}
