package com.example.simona.lab3androidpersistence.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.simona.lab3androidpersistence.Repository.TaskRepository;

public class NetworkChangeReceiver extends BroadcastReceiver {
    private static final String CLASSNAME = "RECEIVER";
    @Override
    public void onReceive(final Context context, final Intent intent) {

        boolean isConnected = NetworkUtil.getConnectivityStatus(context);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction()) || "android.net.wifi.WIFI_STATE_CHANGED".equals(intent.getAction()) ) {
            if (isConnected) {
                Log.d(CLASSNAME, "connection on");
                TaskRepository.sendOfflineTasks();
            } else {
                Log.d(CLASSNAME, "connection of");
            }
        }
    }
}