package com.example.simona.lab3androidpersistence.vo;

public class TokenResponse
{
    private String Token;

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        this.Token = token;
    }

    public TokenResponse(String token) {
        this.Token = token;
    }
}
