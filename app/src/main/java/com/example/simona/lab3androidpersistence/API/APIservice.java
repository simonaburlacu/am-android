package com.example.simona.lab3androidpersistence.API;

import com.example.simona.lab3androidpersistence.Entities.Task;
import com.example.simona.lab3androidpersistence.vo.AccountRequest;
import com.example.simona.lab3androidpersistence.vo.AccountResponse;
import com.example.simona.lab3androidpersistence.vo.Page;
import com.example.simona.lab3androidpersistence.vo.TaskRequest;
import com.example.simona.lab3androidpersistence.vo.TaskVO;
import com.example.simona.lab3androidpersistence.vo.TokenResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface APIservice
{
    String MY_URL = "http://192.168.43.75:50126/";

    @POST("account/login")
    Call<TokenResponse> login(@Body AccountRequest post);

    @GET("tasks")
    Call<Page> getTasks(@Header("Authorization") String authHeader,@Header("Etag") String etagHeader);

    @POST("update")
    Call<TaskVO> update(@Body TaskRequest taskRequest, @Header("Authorization") String authHeader);

}
