package com.example.simona.lab3androidpersistence.vo;

import android.text.BoringLayout;

public class AccountResponse
{
    private String Username;
    private String Password;
    private boolean IsSuccess;

    public void setSuccess(boolean success) {
        IsSuccess = success;
    }

    public boolean isSuccess() {
        return IsSuccess;
    }

    public String getUsername() {
        return Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public AccountResponse(String username, String password, Boolean isSuccess) {
        Username = username;
        Password = password;
        IsSuccess=isSuccess;
    }

    public AccountResponse()
    {

    }
}
