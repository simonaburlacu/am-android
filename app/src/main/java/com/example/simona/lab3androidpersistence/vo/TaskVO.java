package com.example.simona.lab3androidpersistence.vo;

public class TaskVO {
    int Id;
    String Description;
    int Priority;


    public String getDescription() {
        return Description;
    }
    public void setDescription(String description) {
        Description = description;
    }

    public void setPriority(int priority) {
        Priority = priority;
    }
    public int getPriority() {
        return Priority;
    }

    public int getId() {
        return Id;
    }
    public void setId(int id) { Id = id; }

    public TaskVO(int id, String description, int priority) {
        Id = id;
        Description = description;
        Priority = priority;
    }
    public TaskVO(){}
}
