package com.example.simona.lab3androidpersistence.ViewModels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.support.annotation.NonNull;

import com.example.simona.lab3androidpersistence.Database.UserDatabase;
import com.example.simona.lab3androidpersistence.Entities.Token;
import com.example.simona.lab3androidpersistence.Repository.UserRepository;

public class UserViewModel extends ViewModel
{
    private UserRepository userRepository;
    private LiveData<Token> tokenLiveData;

    public UserViewModel(Context context)
    {
        userRepository=UserRepository.getInstance(UserDatabase.getDatabase(context).getUserDao(), UserDatabase.getDatabase(context).getTokenDao());
        tokenLiveData = userRepository.getToken();
    }

    public LiveData<Token> getToken() {
        return tokenLiveData;
    }

    public void login(String username, String password, Function<String,String> callback)
    {
        userRepository.login(username, password,callback);

    }

    public void clearToken()
    {
        userRepository.clearToken();
    }

    public static class Factory implements ViewModelProvider.Factory {
        private final Context ctxt;

        public Factory(Context ctxt) {
            this.ctxt=ctxt.getApplicationContext();
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return((T)new UserViewModel(ctxt));
        }
    }
}
