package com.example.simona.lab3androidpersistence.UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.simona.lab3androidpersistence.Entities.Task;
import com.example.simona.lab3androidpersistence.R;
import java.util.List;

public class TaskListAdapter extends ArrayAdapter<Task> {

    public TaskListAdapter(Context context, List<Task> tasks) {
        super(context, 0, tasks);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Task task = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_task, parent, false);
        }
        TextView text_description = (TextView) convertView.findViewById(R.id.description);
        text_description.setText(task.getDescription());
        return convertView;
    }
}