package com.example.simona.lab3androidpersistence.UI;


import android.arch.core.util.Function;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import com.example.simona.lab3androidpersistence.Entities.Token;
import com.example.simona.lab3androidpersistence.R;
import com.example.simona.lab3androidpersistence.ViewModels.UserViewModel;


public class MainActivity extends AppCompatActivity {
    private EditText text_username;
    private EditText text_password;
    private Button button_login;
    private Button button_signup;
    private CheckBox checkBox_account;
    private TextView text_welcome,text_error;
    private UserViewModel userViewModel;

    private void initControls()
    {
        text_username=(EditText)findViewById(R.id.username);
        text_password=(EditText)findViewById(R.id.password);
        button_login=(Button)findViewById(R.id.login);
        button_signup=(Button)findViewById(R.id.signup);
        checkBox_account=(CheckBox)findViewById(R.id.checkBox);
        text_welcome=(TextView)findViewById(R.id.welcome);
        text_error=(TextView)findViewById(R.id.error);
        text_welcome.setText("Welcome!");
        button_signup.setVisibility(View.GONE);
        button_login.setVisibility(View.VISIBLE);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initControls();

        userViewModel=ViewModelProviders.of(this, new UserViewModel.Factory(getApplicationContext())).get(UserViewModel.class);

        checkBox_account.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    text_error.setVisibility(View.GONE);
                    button_signup.setVisibility(View.VISIBLE);
                    button_login.setVisibility(View.GONE);
                }
                else
                {
                    text_error.setVisibility(View.GONE);
                    button_signup.setVisibility(View.GONE);
                    button_login.setVisibility(View.VISIBLE);
                }
            }
        });

        userViewModel.getToken().observe(this, new Observer<Token>() {
            @Override
            public void onChanged(@Nullable Token token) {
                if(token!=null)
                {
                    Log.d("ACTIVITY","LOG IN OK");
                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Log.d("ACTIVITY", "VALUE TOKEN IS NULL");
                }

        }});

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_error.setVisibility(View.GONE);
                Function<String,String> callback= new Function<String, String>() {
                    @Override
                    public String apply(String input) {
                        Intent intent = new Intent(MainActivity.this, UnauthorizedActivity.class);
                        startActivity(intent);
                        finish();
                        return "unauthorized";
                    }
                };
               userViewModel.login(text_username.getText().toString(), text_password.getText().toString(),callback);

            }
        });
    }
}
