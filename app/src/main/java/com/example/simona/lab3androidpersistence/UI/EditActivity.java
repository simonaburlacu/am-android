package com.example.simona.lab3androidpersistence.UI;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;;
import android.widget.EditText;
import android.widget.TextView;

import com.example.simona.lab3androidpersistence.Entities.Token;
import com.example.simona.lab3androidpersistence.R;
import com.example.simona.lab3androidpersistence.ViewModels.TaskViewModel;
import com.example.simona.lab3androidpersistence.vo.TaskEdit;
import com.example.simona.lab3androidpersistence.vo.TokenVO;

public class EditActivity  extends AppCompatActivity {
    private EditText text_description;
    private Button button_edit;
    private TextView text_id,text_error;
    private TaskViewModel taskViewModel;
    private TokenVO existingToken=null;

    private void initControls() {
        text_description = (EditText) findViewById(R.id.description);
        text_id = (TextView) findViewById(R.id.id_task);
        text_error = (TextView) findViewById(R.id.error);
        button_edit = (Button) findViewById(R.id.edit);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit);
        initControls();
        TaskEdit taskEdit = (TaskEdit) getIntent().getSerializableExtra("TaskEdit");
        existingToken=(TokenVO)  getIntent().getSerializableExtra("Token");
        taskViewModel=ViewModelProviders.of(this, new TaskViewModel.Factory(getApplicationContext())).get(TaskViewModel.class);
        text_id.setText(String.valueOf(taskEdit.getId()));
        text_description.setText(taskEdit.getDescription());

        final Context context=this;
        button_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    editTask();
                    Intent intent = new Intent(context, HomeActivity.class);
                    finish();
                    startActivity(intent);
            }
        });
    }

    public void editTask()
    {
        int id=Integer.parseInt(text_id.getText().toString());
        String newDescription=text_description.getText().toString();
        taskViewModel.editTask(id,newDescription, existingToken);
    }
}
